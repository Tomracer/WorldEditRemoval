
# WorldEditRemoval

WorldEdit removal is a script that removes invisible blocks from a worldedit file. 



## Dependencies

| Name | URL     | Description                |
| :-------- | :------- | :------------------------- |
| `SLPP` | `https://github.com/SirAnthony/slpp` | simple lua-python data structures parser. |

#### Installation using python virtual enviroments 
```bash
  python3 -m venv path/to/new/virtual/environment

  source my_project/bin/activate

  pip install git+https://github.com/SirAnthony/slpp
```
## Run

To run this script simpy execute this command

```bash
  python3 CleanSchematic.py path/to/worldedit/schematic
```


## Known Issues

- Grass counts as a full block. So you can see inside and fall through the Terrain.




## FAQ

#### Which file formats are supported?

Currently only .we files

### Can you show me a example .we file that is supported?

```lua
5:r1="default:stone";r2="default:dirt_with_grass";r3="default:dry_dirt_with_dry_grass";r4="default:stone_with_coal";return {{x=0,y=0,z=0,name=r1},{x=0,y=0,z=1,name=r1},{x=0,y=0,z=2,name=r1},{x=0,y=0,z=3,name=r1},{x=0,y=1,z=0,name=r1},{x=0,y=1,z=1,name=r1},{x=0,y=1,z=2,name=r1},{x=0,y=1,z=3,name=r1},{x=0,y=2,z=0,name=r1},{x=0,y=2,z=1,name=r1},{x=0,y=2,z=2,name=r1},{x=0,y=2,z=3,name=r1},{x=0,y=3,z=0,name=r1},{x=0,y=3,z=1,name=r4},{x=0,y=3,z=2,name=r1},{x=0,y=3,z=3,name=r1},{x=0,y=4,z=0,name=r1},{x=0,y=4,z=1,name=r1},{x=0,y=4,z=2,name=r4},{x=0,y=4,z=3,name=r1},{x=0,y=5,z=0,name=r3},{x=0,y=5,z=1,name=r3},{x=0,y=5,z=2,name=r2},{x=0,y=5,z=3,name=r2},{x=1,y=0,z=0,name=r1},{x=1,y=0,z=1,name=r1},{x=1,y=0,z=2,name=r1},{x=1,y=0,z=3,name=r1},{x=1,y=1,z=0,name=r1},{x=1,y=1,z=1,name=r1},{x=1,y=1,z=2,name=r1},{x=1,y=1,z=3,name=r1},{x=1,y=2,z=0,name=r1},{x=1,y=2,z=1,name=r1},{x=1,y=2,z=2,name=r4},{x=1,y=2,z=3,name=r4},{x=1,y=3,z=0,name=r1},{x=1,y=3,z=1,name=r4},{x=1,y=3,z=2,name=r4},{x=1,y=3,z=3,name=r4},{x=1,y=4,z=0,name=r4},{x=1,y=4,z=1,name=r4},{x=1,y=4,z=2,name=r4},{x=1,y=4,z=3,name=r1},{x=1,y=5,z=0,name=r3},{x=1,y=5,z=1,name=r3},{x=1,y=5,z=2,name=r2},{x=1,y=5,z=3,name=r2},{x=2,y=0,z=0,name=r1},{x=2,y=0,z=1,name=r1},{x=2,y=0,z=2,name=r1},{x=2,y=0,z=3,name=r1},{x=2,y=1,z=0,name=r1},{x=2,y=1,z=1,name=r1},{x=2,y=1,z=2,name=r1},{x=2,y=1,z=3,name=r1},{x=2,y=2,z=0,name=r1},{x=2,y=2,z=1,name=r1},{x=2,y=2,z=2,name=r1},{x=2,y=2,z=3,name=r4},{x=2,y=3,z=0,name=r1},{x=2,y=3,z=1,name=r1},{x=2,y=3,z=2,name=r1},{x=2,y=3,z=3,name=r1},{x=2,y=4,z=0,name=r1},{x=2,y=4,z=1,name=r4},{x=2,y=4,z=2,name=r4},{x=2,y=4,z=3,name=r4},{x=2,y=5,z=0,name=r3},{x=2,y=5,z=1,name=r3},{x=2,y=5,z=2,name=r2},{x=2,y=5,z=3,name=r2}}

```

#### Does it modify the files?

No, the script doesnt touch the original files. It creates a new file and places the modified content in it.


