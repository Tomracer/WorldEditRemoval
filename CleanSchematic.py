from slpp import slpp as lua
from os.path import exists
import argparse, re, sys

parser = argparse.ArgumentParser(description='Remove invisible blocks from a WorldEdit schematic.', )
parser.add_argument('-v', "--verbose", action='store_true', help="Enables verbose logging")
parser.add_argument("-o", "--output", action="store_true", help="print output to console")
parser.add_argument("filename", help="WorldEdit schematic filename")
parser.add_argument("new_filename", help="New WorldEdit schematic filename")
args = parser.parse_args()

def read(filename: str):
  if not exists(filename):
    raise FileNotFoundError(f"[{filename}] does not exist")
  try:
    with open(filename, "r") as file:
      content = file.read()
  except:
    raise PermissionError("Cant access file")
  return content
    
def grep(content):
  content_split = {}
  content_split["unmod"] = re.search(".*return", content)
  content_split["mod"] = re.search("(?<=return).*", content)
  if not (content_split["unmod"] and content_split["mod"]):
    raise NotImplementedError("Not a valid file format. Does your file end with .we or .mts?")
  return content_split

def log(**kwargs):
  if "removed_block" in kwargs and "removed_blocks" in kwargs:
    print("Removed Block {1}: {0}".format(str(kwargs["removed_block"]), kwargs["removed_blocks"]))
  elif "original_table" in kwargs  and "modified_table" in kwargs:
    print("\nDetails:\n  Original Length: {0}\n  New Length: {1}\n  Removed Blocks {2}".format(len(kwargs["original_table"]), len(kwargs["modified_table"]), len(kwargs["original_table"])-len(kwargs["modified_table"])-1))

def sort(lua_table):
  data = lua.decode(lua_table.group())
  data_less = []
  index = 0
  if type(data) != list:
    raise NotImplementedError("Not a valid file format. Does your file end with .we or .mts?")
  for key, value in enumerate(data):
    data_less.append({"x": value["x"], "y": value["y"], "z": value["z"]})
  for key, value in enumerate(data_less):
    if (
        {"x": value["x"]-1, "y": value["y"]  , "z": value["z"]  } in data_less and
        {"x": value["x"]+1, "y": value["y"]  , "z": value["z"]  } in data_less and
        {"x": value["x"]  , "y": value["y"]-1, "z": value["z"]  } in data_less and
        {"x": value["x"]  , "y": value["y"]+1, "z": value["z"]  } in data_less and
        {"x": value["x"]  , "y": value["y"]  , "z": value["z"]-1} in data_less and
        {"x": value["x"]  , "y": value["y"]  , "z": value["z"]+1} in data_less
       ):
       data.pop(key-index)
       if args.verbose:
          log(removed_block=data[key-index], removed_blocks=index)
       index += 1
  return data

def write(filename, clr_table, values):
  try:
    with open(filename, "w") as file:
      data = ''.join(lua.encode(clr_table).split())
      data = re.compile(r'"(r.*?)"').sub(r"\1", data)
      data = data.replace("[\"", "").replace("\"]", "")
      file.write(values.group()+"\n"+data)
      if args.output:
        print("\n"+values.group()+data+"\n")
  except:
    raise NotImplementedError("Something went wrong, check if you execute the script with the necessary permissions.")

def main():
  schematic_parsed = grep(read(args.filename))
  cleaned_schematic = sort(schematic_parsed["mod"])
  if args.verbose:
    log(original_table=lua.decode(schematic_parsed["mod"].group()), modified_table=cleaned_schematic)
  write(args.new_filename, cleaned_schematic, schematic_parsed["unmod"])
  print("Done!")

if __name__ == "__main__":
    main()

